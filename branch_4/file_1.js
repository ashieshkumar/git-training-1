var x = 10;
var y = 10;

try {
  x = y; // ReferenceError: y is not defined
} catch (error) {
  console.error("Error:", error.message);
}
