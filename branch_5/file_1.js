let a = 10;

function increaseValue() {
  a++;
  console.log(a);
}

increaseValue(); // Outputs: 11

increaseValue(); // Outputs: 12

console.log(a); // Outputs: 12

const b = increaseValue;

b(); // Outputs: 13
